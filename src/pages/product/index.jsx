import Header from 'components/Header';
import { Box, useMediaQuery } from '@mui/material';
import React from 'react';
import { useGetProductsQuery } from 'state/api';
import ProductItem from './productItem';

const Product = () => {
  const { data, isLoading } = useGetProductsQuery();
  const isNonMobile = useMediaQuery('(min-width: 600px)');

  return (
    <Box m="1.5rem 2.5rem">
      <Header title="PRODUCTS" subtitle="See your list of products" />
      {
        (data || !isLoading)
          ? (
            <Box
              mt="20px"
              display="grid"
              gridTemplateColumns="repeat(4, minmax(0, 1fr))"
              justifyContent="space-between"
              rowGap="20px"
              columnGap="1.33%"
              sx={{
                "& > div": {gridColumn: isNonMobile ? undefined : "span 4"}
              }}
            >
              {
                data.map(({
                  _id,
                  category,
                  description,
                  name,
                  price,
                  rating,
                  stat,
                  supply,
                }) => (
                  <ProductItem
                    key={_id}
                    _id={_id}
                    category={category}
                    description={description}
                    name={name}
                    price={price}
                    rating={rating}
                    stat={stat}
                    supply={supply}
                  />
                ))
              }
            </Box>
          )
          : (<>Loading...</>)
      }
    </Box>
  );
};

export default Product;
