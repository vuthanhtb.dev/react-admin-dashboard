import { configureStore } from '@reduxjs/toolkit';
import globalReducers from 'state';
import { api } from 'state/api';

const store = configureStore({
  reducer: {
    global: globalReducers,
    [api.reducerPath]: api.reducer
  },
  middleware: (getDefault) => getDefault().concat(api.middleware)
});

export default store;
