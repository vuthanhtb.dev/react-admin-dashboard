import { useMemo } from 'react';
import { useSelector } from 'react-redux';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import { CssBaseline, ThemeProvider, createTheme } from '@mui/material';
import { themeSettings } from 'theme';
import Dashboard from 'pages/dashboard';
import Layout from 'pages/layout';
import Product from 'pages/product';
import Customer from 'pages/customer';
import Transaction from 'pages/transaction';
import Geography from 'pages/geography';
import Overview from 'pages/overview';
import Daily from 'pages/daily';
import Monthly from 'pages/monthly';
import Breakdown from 'pages/breakdown';
import Admin from 'pages/admin';
import Performance from 'pages/performance';

function App() {
  const mode = useSelector((state) => state.global.mode);
  const theme = useMemo(() => createTheme(themeSettings(mode)), [mode]); 
  
  return (
    <div className="app">
      <BrowserRouter>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <Routes>
            <Route element={<Layout />}>
              <Route path="/" element={<Navigate to="/dashboard" replace/>} />
              <Route path="/dashboard" element={<Dashboard />}/>
              <Route path="/products" element={<Product />}/>
              <Route path="/customers" element={<Customer />}/>
              <Route path="/transactions" element={<Transaction />}/>
              <Route path="/geography" element={<Geography />}/>
              <Route path="/overview" element={<Overview />}/>
              <Route path="/daily" element={<Daily />}/>
              <Route path="/monthly" element={<Monthly />}/>
              <Route path="/breakdown" element={<Breakdown />}/>
              <Route path="/admin" element={<Admin />}/>
              <Route path="/performance" element={<Performance />}/>
            </Route>
          </Routes>
        </ThemeProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
