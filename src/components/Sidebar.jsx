import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import {
  Box,
  Divider,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Typography,
  useTheme
} from '@mui/material';
import { ChevronLeft, ChevronRightOutlined, SettingsOutlined } from '@mui/icons-material';
import FlexBetween from 'components/FlexBetween';
import navItems from 'constants/NavItemConstant';
import profileImage from 'assets/images/profile.jpg';

const Sidebar = (props) => {
  const {
    user,
    isNonMobile,
    isSidebarOpen,
    setIsSidebarOpen,
    drawerWidth
  } = props;

  const { pathname } = useLocation();
  const navigate = useNavigate();
  const theme = useTheme();

  const [active, setActive] = useState('');

  useEffect(() => {
    setActive(pathname.substring(1));
  }, [pathname]);

  return (
    <Box component="nav">
      {
        isSidebarOpen && (
          <Drawer
            open={isSidebarOpen}
            onClose={() => setIsSidebarOpen(false)}
            variant="persistent"
            anchor="left"
            sx={{
              width: drawerWidth,
              "& .MuiDrawer-paper": {
                colors: theme.palette.secondary[200],
                backgroundColor: theme.palette.background.alt,
                boxSizing: "border-box",
                width: drawerWidth,
                borderWidth: isNonMobile ? 0 : "2px"
              }
            }}
          >
            <Box width="100%">
              <Box m="1.5rem 2rem 2rem 3rem">
                <FlexBetween color={theme.palette.secondary.main}>
                  <Box display="flex" alignItems="center" gap="0.5rem">
                    <Typography variant="h4" fontWeight="bold">
                      REACT ADMIN
                    </Typography>
                    {
                      !isNonMobile && (
                        <IconButton onClick={() => setIsSidebarOpen(!isSidebarOpen)}>
                          <ChevronLeft />
                        </IconButton>
                      )
                    }
                  </Box>
                </FlexBetween>
              </Box>
              <List>
                {
                  navItems.map(({text, icon}) => {
                    if (!icon) {
                      return (
                        <Typography key={text} sx={{m: "2.25rem 0 1rem 3rem"}}>
                          {text}
                        </Typography>
                      );
                    }

                    const lcText = text.toLowerCase();

                    return (
                      <ListItem key={text} disablePadding>
                        <ListItemButton
                          onClick={() => {
                            navigate(`/${lcText}`);
                            setActive(lcText);
                          }}
                          sx={{
                            backgroundColor: active === lcText
                              ? theme.palette.secondary[300]
                              : "transparent",
                            color: active === lcText
                              ? theme.palette.secondary[600]
                              : theme.palette.secondary[100]
                          }}
                        >
                          <ListItemIcon
                            sx={{
                              ml: "2rem",
                              color: active === lcText
                                ? theme.palette.secondary[600]
                                : theme.palette.secondary[200]
                            }}
                          >
                            {icon}
                          </ListItemIcon>
                          <ListItemText primary={text}>
                            {
                              active === lcText && (
                                <ChevronRightOutlined sx={{ml: "auto"}}/>
                              )
                            }
                          </ListItemText>
                        </ListItemButton>
                      </ListItem>
                    );
                  })
                }
              </List>
            </Box>

            <Box position="absolute" bottom="2rem">
              <Divider/>
              <FlexBetween textTransform="none" gap="1rem" m="1.5rem 2rem 0 3rem">
                <Box
                  component="img"
                  alt="profile"
                  src={profileImage}
                  height="40px"
                  width="40px"
                  borderRadius="50%"
                  sx={{objectFit: "cover"}}
                />
                <Box textAlign="left">
                  <Typography
                    fontWeight="bold"
                    fontSize="0.9rem"
                    sx={{color: theme.palette.secondary[100]}}
                  >
                    {user?.name}
                  </Typography>
                  <Typography
                    fontSize="0.8rem"
                    sx={{color: theme.palette.secondary[200]}}
                  >
                    {user?.occupation}
                  </Typography>
                </Box>
                <SettingsOutlined
                  sx={{
                    color: theme.palette.secondary[300],
                    fontSize: 25
                  }}
                />
              </FlexBetween>
            </Box>
          </Drawer>
        )
      }
    </Box>
  );
};

export default Sidebar;
