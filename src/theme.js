export const tokensDark = {
  grey: {
    0: "#FFFFFF",
    10: "#F6F6F6",
    50: "#F0F0F0",
    100: "#E0E0E0",
    200: "#C2C2C2",
    300: "#A3A3A3",
    400: "#858585",
    500: "#666666",
    600: "#525252",
    700: "#3D3D3D",
    800: "#292929",
    900: "#141414",
    1000: "#000000"
  },
  primary: {
    100: "#D3D4DE",
    200: "#A6A9BE",
    300: "#7A7F9D",
    400: "#4D547D",
    500: "#21295C",
    600: "#191F45",
    700: "#141937",
    800: "#0D1025",
    900: "#070812"
  },
  secondary: {
    50: "#F0F0F0",
    100: "#FFF6E0",
    200: "#FFEDC2",
    300: "#FFE3A3",
    400: "#FFDA85",
    500: "#FFD166",
    600: "#CCA752",
    700: "#997D3D",
    800: "#665429",
    900: "#332A14"
  },
};

const reverseTokens = (tokenDark) => {
  const reversedTokens = {};
  Object.entries(tokenDark).forEach(([key, val]) => {
    const keys = Object.keys(val);
    const values = Object.values(val);
    const length = keys.length;
    const reversedObj = {};
    for (let index = 0; index < length; index++) {
      reversedObj[keys[index]] = values[length - 1 - index];
    }
    reversedTokens[key] = reversedObj;
  });

  return reversedTokens;
};

export const tokensLight = reverseTokens(tokensDark);

export const themeSettings = (mode) => {
  return {
    palette: {
      mode,
      ...('dark' === mode
      ? {
        primary: {
          ...tokensDark.primary,
          main: tokensDark.primary[400],
          light: tokensDark.primary[400],
        },
        secondary: {
          ...tokensDark.secondary,
          main: tokensDark.secondary[300],
        },
        neutral: {
          ...tokensDark.grey,
          main: tokensDark.grey[500],
        },
        background: {
          default: tokensDark.primary[600],
          alt: tokensDark.primary[500],
        },
      }
      : {
        primary: {
          ...tokensDark.primary,
          main: tokensDark.grey[50],
          light: tokensDark.grey[100],
        },
        secondary: {
          ...tokensDark.secondary,
          main: tokensDark.secondary[600],
          light: tokensDark.secondary[700],
        },
        neutral: {
          ...tokensDark.grey,
          main: tokensDark.grey[500],
        },
        background: {
          default: tokensDark.grey[0],
          alt: tokensDark.grey[50],
        },
      })
    },
    typography: {
      fontFamily: ['Inter', 'sans-serif'].join(','),
      fontSize: 12,
      h1: {
        fontFamily: ['Inter', 'sans-serif'].join(','),
        fontSize: 40,
      },
      h2: {
        fontFamily: ['Inter', 'sans-serif'].join(','),
        fontSize: 32,
      },
      h3: {
        fontFamily: ['Inter', 'sans-serif'].join(','),
        fontSize: 24,
      },
      h4: {
        fontFamily: ['Inter', 'sans-serif'].join(','),
        fontSize: 20,
      },
      h5: {
        fontFamily: ['Inter', 'sans-serif'].join(','),
        fontSize: 16,
      },
      h6: {
        fontFamily: ['Inter', 'sans-serif'].join(','),
        fontSize: 14,
      },
    }
  };
};
