import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const api = createApi({
  baseQuery: fetchBaseQuery({baseUrl: process.env.REACT_APP_BASE_URL}),
  reducerPath: 'adminAPI',
  tagTypes: [
    'User',
    'Product',
    'Customer',
    'Transactions',
    'Geography',
    'Sales',
    'Admin',
    'Performance',
    'Dashboard'
  ],
  endpoints: (build) => ({
    getUserById: build.query({
      query: (id) => `/general/users/${id}`,
      providesTags: ['User'],
    }),
    getProducts: build.query({
      query: () => '/client/products',
      providesTags: ['Product'],
    }),
    getCustomers: build.query({
      query: () => '/client/customers',
      providesTags: ['Customer'],
    }),
    getTransactions: build.query({
      query: ({page, pageSize, sort, search}) => ({
        url: '/client/transactions',
        method: 'GET',
        params: {page, pageSize, sort, search}
      }),
      providesTags: ['Transactions'],
    }),
    getGeography: build.query({
      query: () => '/client/geography',
      providesTags: ['Geography'],
    }),
    getSales: build.query({
      query: () => '/sales/sales',
      providesTags: ['Sales'],
    }),
    getAdmins: build.query({
      query: () => '/management/admin',
      providesTags: ['Admin'],
    }),
    getUserPerformance: build.query({
      query: (id) => `/management/performance/${id}`,
      providesTags: ['Performance'],
    }),
    getDashboard: build.query({
      query: () => '/general/dashboard',
      providesTags: ['Dashboard'],
    }),
  })
});

export const {
  useGetUserByIdQuery,
  useGetProductsQuery,
  useGetCustomersQuery,
  useGetTransactionsQuery,
  useGetGeographyQuery,
  useGetSalesQuery,
  useGetAdminsQuery,
  useGetUserPerformanceQuery,
  useGetDashboardQuery
} = api;
